package com.mobyta.videoapp;

import android.app.Activity;
import android.content.Intent;

import com.mobyta.videoapp.tools.EventDialog;
import com.mobyta.videoapp.tools.utils;
import com.mobyta.videoapp.videorecorder.FFmpegRecorderActivity;

/**
 * Created by Sanchit on 21-02-2016.
 */
public class AppUtils {
    public static void showOptionSelectorDialog(final Activity activity) {
        new EventDialog(activity, "Select one option", "How would you like to record", "With music", "Without music", new EventDialog.DialogListener() {
            @Override
            public void onDialogEvent(final boolean mainAction) {
                // Record with Music
                new EventDialog(activity, "Select one option", "What would you like to record", "Audio", "Video", new EventDialog.DialogListener() {
                    @Override
                    public void onDialogEvent(boolean action) {
                        if (action) {
                            if (mainAction) {
                                // TODO Record Audio with music
                            } else {
                                // TODO Record Audio without music

                            }
                        } else {
                            if (mainAction) {
                                // Record Video with music
                                utils.openVideoPicker(activity);
                            } else {
                                // TODO Record Video without music
                                activity.startActivity(new Intent(activity, FFmpegRecorderActivity.class));
                            }
                        }
                    }
                });
            }
        });
    }
}
