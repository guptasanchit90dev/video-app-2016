package com.mobyta.videoapp.tools;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

/**
 * Created by Sanchit on 21-02-2016.
 */
public class EventDialog {
    public EventDialog(Context context, String title, String message, String positive, String negative, final DialogListener dialogListener) {
        AlertDialog alertDialog = new AlertDialog.Builder(
                context)
                .setPositiveButton(positive, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (dialogListener != null) {
                            dialogListener.onDialogEvent(true);
                        }
                    }
                })
                .setNegativeButton(negative, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (dialogListener != null) {
                            dialogListener.onDialogEvent(false);
                        }
                    }
                })
                .setTitle(title)
                .setMessage(message)
                .create();
        alertDialog.show();
    }

    public interface DialogListener {
        void onDialogEvent(boolean action);
    }
}
