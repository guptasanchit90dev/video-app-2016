package com.mobyta.videoapp;

import android.media.MediaCodec;
import android.media.MediaCodecInfo;
import android.media.MediaExtractor;
import android.media.MediaFormat;
import android.media.MediaMetadataRetriever;
import android.media.MediaMuxer;
import android.os.Environment;
import android.util.Log;

import com.coremedia.iso.boxes.Container;
import com.googlecode.mp4parser.FileDataSourceImpl;
import com.googlecode.mp4parser.authoring.Movie;
import com.googlecode.mp4parser.authoring.Track;
import com.googlecode.mp4parser.authoring.builder.DefaultMp4Builder;
import com.googlecode.mp4parser.authoring.container.mp4.MovieCreator;
import com.googlecode.mp4parser.authoring.tracks.AppendTrack;
import com.googlecode.mp4parser.authoring.tracks.MP3TrackImpl;
import com.mpatric.mp3agic.InvalidDataException;
import com.mpatric.mp3agic.Mp3File;
import com.mpatric.mp3agic.NotSupportedException;
import com.mpatric.mp3agic.UnsupportedTagException;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.BufferOverflowException;
import java.nio.ByteBuffer;
import java.nio.channels.WritableByteChannel;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by sanchit.gupta on 3/14/2016.
 */
public class MuxUtils {

    /*
    Muxing mp4 video file with mp3 audio file
    @param  videoFile mp4 video file no audio
    @param  audioFile mp3 audi file
    @param  outputFile final mp4 file with audio
     @return true if success
     */

    public boolean muxVideoAndAudio(final String videoFile, final String audioFile, final String outputFile) {


        String intermediatemp3 = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_MOVIES).getPath() + "/imageToVideo/converted.mp3";
        Movie video = null;
        try {
            video = new MovieCreator().build(videoFile);

        } catch (RuntimeException e) {
            e.printStackTrace();
            return false;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        } catch (Exception e) {
            e.printStackTrace();
        }


        try {


            Mp3File mp3file = new Mp3File(audioFile);
            if (mp3file.hasId3v1Tag()) {
                mp3file.removeId3v1Tag();
                Log.d("MP3agic", "removeId3v1Tag");
            }
            if (mp3file.hasId3v2Tag()) {
                mp3file.removeId3v2Tag();
                Log.d("MP3agic", "removeId3v2Tag");
            }
            if (mp3file.hasCustomTag()) {
                mp3file.removeCustomTag();
                Log.d("MP3agic", "removeCustomTag");
            }
            mp3file.save(intermediatemp3);


            MP3TrackImpl mp3Track = new MP3TrackImpl(new FileDataSourceImpl(intermediatemp3));


            video.addTrack(mp3Track);


        } catch (IOException e) {
            e.printStackTrace();
            return false;
        } catch (NullPointerException e) {
            e.printStackTrace();
            return false;
        } catch (Exception e) {
            e.printStackTrace();
        }


        Container out = new DefaultMp4Builder().build(video);
        FileOutputStream fos;
        try {
            fos = new FileOutputStream(outputFile);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return false;
        }
        BufferedWritableFileByteChannel byteBufferByteChannel = new BufferedWritableFileByteChannel(fos);
        try {
            out.writeContainer(byteBufferByteChannel);
            byteBufferByteChannel.close();
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }


        return true;
    }

    private static class BufferedWritableFileByteChannel implements WritableByteChannel {
        private static final int BUFFER_CAPACITY = 1000000;
        private boolean isOpen = true;
        private final OutputStream outputStream;
        private final ByteBuffer byteBuffer;
        private final byte[] rawBuffer = new byte[BUFFER_CAPACITY];

        private BufferedWritableFileByteChannel(OutputStream outputStream) {
            this.outputStream = outputStream;
            this.byteBuffer = ByteBuffer.wrap(rawBuffer);
        }

        @Override
        public int write(ByteBuffer inputBuffer) throws IOException {
            int inputBytes = inputBuffer.remaining();
            if (inputBytes > byteBuffer.remaining()) {
                dumpToFile();
                byteBuffer.clear();
                if (inputBytes > byteBuffer.remaining()) {
                    throw new BufferOverflowException();
                }
            }
            byteBuffer.put(inputBuffer);
            return inputBytes;
        }

        @Override
        public boolean isOpen() {
            return isOpen;
        }

        @Override
        public void close() throws IOException {
            dumpToFile();
            isOpen = false;
        }

        private void dumpToFile() {
            try {
                outputStream.write(rawBuffer, 0, byteBuffer.position());
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    /*
    combine 2 mp4 file to single mp4 file only video no audio
        @param  videoFile1 mp4 video file no audio
        @param  videoFile2 mp4 video file no audio
        @param  outputFilePath final mp4 file without audio
        @return true if success
     */
    public boolean appendVideo(final String videoFile1, final String videoFile2, final String outputFilePath) {
        try {

            Movie[] inMovies = new Movie[]{
                    MovieCreator.build(videoFile1),
                    MovieCreator.build(videoFile2)};


            List<Track> videoTracks = new LinkedList<Track>();
            List<Track> audioTracks = new LinkedList<Track>();
            for (Movie m : inMovies) {
                for (Track t : m.getTracks()) {
                    if (t.getHandler().equals("soun")) {
                        audioTracks.add(t);
                    }
                    if (t.getHandler().equals("vide")) {
                        videoTracks.add(t);
                    }
                }
            }
            Movie result = new Movie();
            if (audioTracks.size() > 0) {
                result.addTrack(new AppendTrack(audioTracks.toArray(new Track[audioTracks.size()])));
            }
            if (videoTracks.size() > 0) {
                result.addTrack(new AppendTrack(videoTracks.toArray(new Track[videoTracks.size()])));
            }

            Container out = new DefaultMp4Builder().build(result);
            FileOutputStream fos = new FileOutputStream(new File(outputFilePath));
            out.writeContainer(fos.getChannel());
            fos.close();
        } catch (Exception e) {
            return false;
        }

        return true;
    }


    /*
    Striping audio from input mp4 file and create outputfile mp4 file witout audio
    @param  inputFilename mp4 video file with audio
    @param  outputFilePath final mp4 file without audio
    @return true if success
     */


    public boolean stripAudio(String inputFilename, String outputFilePath) {
        try {


            Movie m = MovieCreator.build(inputFilename);


            List<Track> videoTracks = new LinkedList<Track>();
            List<Track> audioTracks = new LinkedList<Track>();

            for (Track t : m.getTracks()) {
                if (t.getHandler().equals("soun")) {
                    audioTracks.add(t);
                }
                if (t.getHandler().equals("vide")) {
                    videoTracks.add(t);
                }
            }

            Movie result = new Movie();
            if (videoTracks.size() > 0) {
                result.addTrack(new AppendTrack(videoTracks.toArray(new Track[videoTracks.size()])));

            }

            Container out = new DefaultMp4Builder().build(result);

            FileOutputStream fos = new FileOutputStream(new File(outputFilePath));
            out.writeContainer(fos.getChannel());
            fos.close();

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    /*
    converting wav files to m4a
     @param  inputFilename mp4 wav file
        @param  outputFilename final m4a file
     */
    public void convertWavToM4a(final String inputFilename, final String outputFilename) throws FileNotFoundException, IOException, UnsupportedTagException, NotSupportedException, InvalidDataException {
        final String COMPRESSED_AUDIO_FILE_MIME_TYPE = "audio/mp4a-latm";
        final int COMPRESSED_AUDIO_FILE_BIT_RATE = 320000; // 64kbps
        final int SAMPLING_RATE = 22050;
        final int BUFFER_SIZE = 22050;
        final int CODEC_TIMEOUT_IN_MS = 5000;
        String LOGTAG = "CONVERT AUDIO";


        File inputFile = new File(inputFilename);
        FileInputStream fis = new FileInputStream(inputFile);
        File outputFile = new File(outputFilename);
        if (outputFile.exists()) outputFile.delete();
        MediaMuxer mux = new MediaMuxer(outputFile.getAbsolutePath(), MediaMuxer.OutputFormat.MUXER_OUTPUT_MPEG_4);
        MediaFormat outputFormat = MediaFormat.createAudioFormat(COMPRESSED_AUDIO_FILE_MIME_TYPE, SAMPLING_RATE, 1);
        outputFormat.setInteger(MediaFormat.KEY_AAC_PROFILE, MediaCodecInfo.CodecProfileLevel.AACObjectLC);
        outputFormat.setInteger(MediaFormat.KEY_BIT_RATE, COMPRESSED_AUDIO_FILE_BIT_RATE);
        outputFormat.setInteger(MediaFormat.KEY_MAX_INPUT_SIZE, 16384);
        MediaCodec codec = MediaCodec.createEncoderByType(COMPRESSED_AUDIO_FILE_MIME_TYPE);
        codec.configure(outputFormat, null, null, MediaCodec.CONFIGURE_FLAG_ENCODE);
        codec.start();
        ByteBuffer[] codecInputBuffers = codec.getInputBuffers(); // Note: Array of buffers
        ByteBuffer[] codecOutputBuffers = codec.getOutputBuffers();
        MediaCodec.BufferInfo outBuffInfo = new MediaCodec.BufferInfo();
        byte[] tempBuffer = new byte[BUFFER_SIZE];
        boolean hasMoreData = true;
        double presentationTimeUs = 0;
        int audioTrackIdx = 0;
        int totalBytesRead = 0;
        int percentComplete = 0;
        do {
            int inputBufIndex = 0;
            while (inputBufIndex != -1 && hasMoreData) {
                inputBufIndex = codec.dequeueInputBuffer(CODEC_TIMEOUT_IN_MS);

                if (inputBufIndex >= 0) {
                    ByteBuffer dstBuf = codecInputBuffers[inputBufIndex];
                    dstBuf.clear();

                    int bytesRead = fis.read(tempBuffer, 0, dstBuf.limit());

                    if (bytesRead == -1) { // -1 implies EOS
                        hasMoreData = false;
                        codec.queueInputBuffer(inputBufIndex, 0, 0, (long) presentationTimeUs, MediaCodec.BUFFER_FLAG_END_OF_STREAM);
                    } else {
                        totalBytesRead += bytesRead;
                        dstBuf.put(tempBuffer, 0, bytesRead);
                        codec.queueInputBuffer(inputBufIndex, 0, bytesRead, (long) presentationTimeUs, 0);
                        presentationTimeUs = 1000000l * (totalBytesRead / 2) / SAMPLING_RATE;
                    }
                }
            }
            int outputBufIndex = 0;
            while (outputBufIndex != MediaCodec.INFO_TRY_AGAIN_LATER) {
                outputBufIndex = codec.dequeueOutputBuffer(outBuffInfo, CODEC_TIMEOUT_IN_MS);
                if (outputBufIndex >= 0) {
                    ByteBuffer encodedData = codecOutputBuffers[outputBufIndex];
                    encodedData.position(outBuffInfo.offset);
                    encodedData.limit(outBuffInfo.offset + outBuffInfo.size);
                    if ((outBuffInfo.flags & MediaCodec.BUFFER_FLAG_CODEC_CONFIG) != 0 && outBuffInfo.size != 0) {
                        codec.releaseOutputBuffer(outputBufIndex, false);
                    } else {
                        mux.writeSampleData(audioTrackIdx, codecOutputBuffers[outputBufIndex], outBuffInfo);
                        codec.releaseOutputBuffer(outputBufIndex, false);
                    }
                } else if (outputBufIndex == MediaCodec.INFO_OUTPUT_FORMAT_CHANGED) {
                    outputFormat = codec.getOutputFormat();

                    audioTrackIdx = mux.addTrack(outputFormat);
                    mux.start();
                } else if (outputBufIndex == MediaCodec.INFO_OUTPUT_BUFFERS_CHANGED) {

                } else if (outputBufIndex == MediaCodec.INFO_TRY_AGAIN_LATER) {
                    // NO OP
                } else {

                }
            }
            percentComplete = (int) Math.round(((float) totalBytesRead / (float) inputFile.length()) * 100.0);

        } while (outBuffInfo.flags != MediaCodec.BUFFER_FLAG_END_OF_STREAM);
        fis.close();
        mux.stop();
        mux.release();


    }


    public void convertAudio(String filename) throws IOException, NotSupportedException, InvalidDataException, UnsupportedTagException {


        final String COMPRESSED_AUDIO_FILE_MIME_TYPE = "audio/mp4a-latm";
        final int COMPRESSED_AUDIO_FILE_BIT_RATE = 320000; // 64kbps
        final int SAMPLING_RATE = 22050;


        String outputpath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MUSIC).getPath() + "/converted.m4a";


        String intermediatemp3 = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_MUSIC).getPath() + "/intermediatemp3.mp3";


        Mp3File mp3file = new Mp3File(filename);
        if (mp3file.hasId3v1Tag()) {
            mp3file.removeId3v1Tag();
            Log.d("MP3agic", "removeId3v1Tag");
        }
        if (mp3file.hasId3v2Tag()) {
            mp3file.removeId3v2Tag();
            Log.d("MP3agic", "removeId3v2Tag");
        }
        if (mp3file.hasCustomTag()) {
            mp3file.removeCustomTag();
            Log.d("MP3agic", "removeCustomTag");
        }
        mp3file.save(intermediatemp3);


        MediaExtractor extractor = new MediaExtractor();
        extractor.setDataSource(intermediatemp3);


        int trackCount = extractor.getTrackCount();

        MediaMuxer muxer;
        muxer = new MediaMuxer(outputpath, MediaMuxer.OutputFormat.MUXER_OUTPUT_MPEG_4);

        HashMap<Integer, Integer> indexMap = new HashMap<Integer, Integer>(trackCount);

        extractor.selectTrack(0);
        MediaFormat outputFormat = MediaFormat.createAudioFormat(COMPRESSED_AUDIO_FILE_MIME_TYPE, SAMPLING_RATE, 1);
        outputFormat.setInteger(MediaFormat.KEY_AAC_PROFILE, MediaCodecInfo.CodecProfileLevel.AACObjectLC);
        outputFormat.setInteger(MediaFormat.KEY_BIT_RATE, COMPRESSED_AUDIO_FILE_BIT_RATE);
        outputFormat.setInteger(MediaFormat.KEY_MAX_INPUT_SIZE, 16384);

        int dstIndex = muxer.addTrack(outputFormat);
        indexMap.put(0, dstIndex);


        boolean sawEOS = false;
        int bufferSize = 32000;
        int frameCount = 0;
        int offset = 100;
        ByteBuffer dstBuf = ByteBuffer.allocate(bufferSize);
        MediaCodec.BufferInfo bufferInfo = new MediaCodec.BufferInfo();


        muxer.start();
        while (!sawEOS) {
            bufferInfo.offset = offset;
            bufferInfo.size = extractor.readSampleData(dstBuf, offset);
            if (bufferInfo.size < 0) {

                sawEOS = true;
                bufferInfo.size = 0;
            } else {
                bufferInfo.presentationTimeUs = extractor.getSampleTime();
                bufferInfo.flags = extractor.getSampleFlags();
                int trackIndex = extractor.getSampleTrackIndex();
                muxer.writeSampleData(indexMap.get(trackIndex), dstBuf,
                        bufferInfo);
                extractor.advance();
                frameCount++;

            }
        }
//        muxer.stop();
        //    muxer.release();

        return;
    }


    /*
    @return duration of mp3 file in seconds
     */
    public static long getMp3Duration(String filename) {
        MediaMetadataRetriever metaRetriever = new MediaMetadataRetriever();
        metaRetriever.setDataSource(filename);
        return (Long.parseLong(metaRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION)) / 1000);
    }

}
