package com.mobyta.videoapp.floatingbar;

import android.graphics.Point;
import android.graphics.PointF;
import android.view.animation.Animation;
import android.view.animation.Transformation;

/**
 * Created by Sanchit on 16-02-2016.
 */
public class ArcTranslateAnimation extends Animation {
    private int mCenterXType;
    private int mCenterYType;
    private float mCenterXValue;
    private float mCenterYValue;
    private float mRadius;
    private Point mStartPoint;
    private float mDeltaRad;
    private float mStartRad;
    private float mEndRad;

    public ArcTranslateAnimation(int startDegrees, int endDegrees, int centerXType, float centerXValue, int centerYType, float centerYValue) {
        this.mStartRad = (float)((double)startDegrees * 3.141592653589793D / 180.0D);
        this.mEndRad = (float)((double)endDegrees * 3.141592653589793D / 180.0D);
        this.mCenterXType = centerXType;
        this.mCenterXValue = centerXValue;
        this.mCenterYType = centerYType;
        this.mCenterYValue = centerYValue;
    }

    private PointF getArcPoint(float interpolatedTime) {
        float rad = this.mStartRad + (this.mEndRad - this.mStartRad) * interpolatedTime + this.mDeltaRad;
        float dx = (float)(Math.cos((double)rad) * (double)this.mRadius);
        float dy = (float)(Math.sin((double)rad) * (double)this.mRadius);
        return new PointF(dx - (float)this.mStartPoint.x, dy - (float)this.mStartPoint.y);
    }

    protected void applyTransformation(float interpolatedTime, Transformation t) {
        PointF p = this.getArcPoint(interpolatedTime);
        t.getMatrix().postTranslate(p.x, p.y);
    }

    public void initialize(int width, int height, int parentWidth, int parentHeight) {
        super.initialize(width, height, parentWidth, parentHeight);
        float fromX = this.resolveSize(0, 0.0F, width, parentWidth);
        float fromY = this.resolveSize(0, 0.0F, height, parentHeight);
        float centerX = this.resolveSize(this.mCenterXType, this.mCenterXValue, width, parentWidth);
        float centerY = this.resolveSize(this.mCenterYType, this.mCenterYValue, height, parentHeight);
        this.mDeltaRad = (float)Math.atan2((double)(fromY - centerY), (double)(fromX - centerX));
        this.mRadius = (float)Math.sqrt(Math.pow((double)(fromX - centerX), 2.0D) + Math.pow((double)(fromY - centerY), 2.0D));
        this.mStartPoint = new Point((int)(fromX - centerX), (int)(fromY - centerY));
    }
}

