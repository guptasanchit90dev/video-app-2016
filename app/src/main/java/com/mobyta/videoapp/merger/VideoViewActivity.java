package com.mobyta.videoapp.merger;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.media.MediaScannerConnection;
import android.net.ParseException;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.mobyta.videoapp.R;

import java.io.File;
import java.util.concurrent.TimeUnit;


public class VideoViewActivity extends Activity implements OnSeekBarChangeListener {
    Button btnDelete;
    Button btnPlayVideo;
    Button btnShare;
    int duration = 0;
    Handler handler = new Handler();
    ImageView iVFrame;
    boolean isPlay;
    OnClickListener onclickDeleteVideo = new OnClickListener() {
        public void onClick(View v) {
            if (VideoViewActivity.this.videoview != null && VideoViewActivity.this.videoview.isPlaying()) {
                VideoViewActivity.this.videoview.pause();
                VideoViewActivity.this.handler.removeCallbacks(VideoViewActivity.this.seekrunnable);
            }
            File appmusic = new File(VideoViewActivity.this.videoPath);
            if (appmusic.exists()) {
                appmusic.delete();
            }
            MediaScannerConnection.scanFile(VideoViewActivity.this, new String[]{VideoViewActivity.this.videoPath}, null, null);
            VideoViewActivity.this.pd = new ProgressDialog(VideoViewActivity.this);
            VideoViewActivity.this.pd.setMessage("Deleting Video...");
            VideoViewActivity.this.pd.show();
            VideoViewActivity.this.handler.postDelayed(VideoViewActivity.this.runnbledelet, 2000);
        }
    };
    OnClickListener onclickbtnshareall = new OnClickListener() {
        public void onClick(View v) {
            if (VideoViewActivity.this.videoview != null && VideoViewActivity.this.videoview.isPlaying()) {
                VideoViewActivity.this.videoview.pause();
                VideoViewActivity.this.handler.removeCallbacks(VideoViewActivity.this.seekrunnable);
            }
            Intent shareIntent = new Intent("android.intent.action.SEND");
            shareIntent.setType("video/*");
            shareIntent.putExtra("android.intent.extra.STREAM", Uri.parse("file:///" + VideoViewActivity.this.videoPath));
            VideoViewActivity.this.startActivity(Intent.createChooser(shareIntent, "Share Video"));
        }
    };
    OnClickListener onclickplayvideo = new OnClickListener() {
        public void onClick(View v) {
            if (!VideoViewActivity.this.isPlay) {
                VideoViewActivity.this.isPlay = true;
                if (VideoViewActivity.this.videoview != null) {
                    VideoViewActivity.this.videoview.seekTo(0);
                    VideoViewActivity.this.videoview.start();
                    VideoViewActivity.this.handler.postDelayed(VideoViewActivity.this.seekrunnable, 500);
                    VideoViewActivity.this.btnPlayVideo.setVisibility(8);
                }
            } else if (VideoViewActivity.this.videoview == null) {
            } else {
                if (VideoViewActivity.this.videoview.isPlaying()) {
                    VideoViewActivity.this.handler.removeCallbacks(VideoViewActivity.this.seekrunnable);
                    VideoViewActivity.this.videoview.pause();
                    VideoViewActivity.this.btnPlayVideo.setVisibility(0);
                    return;
                }
                VideoViewActivity.this.videoview.start();
                VideoViewActivity.this.handler.postDelayed(VideoViewActivity.this.seekrunnable, 500);
                VideoViewActivity.this.btnPlayVideo.setVisibility(8);
            }
        }
    };
    OnClickListener onclicksharevideo = new OnClickListener() {
        public void onClick(View v) {
            if (VideoViewActivity.this.videoview != null && VideoViewActivity.this.videoview.isPlaying()) {
                VideoViewActivity.this.videoview.pause();
                VideoViewActivity.this.handler.removeCallbacks(VideoViewActivity.this.seekrunnable);
            }
            Intent sharingIntent = new Intent("android.intent.action.SEND");
            sharingIntent.putExtra("android.intent.extra.SUBJECT", "Video Maker");
            sharingIntent.setType("video/*");
            sharingIntent.putExtra("android.intent.extra.STREAM", Uri.fromFile(new File(VideoViewActivity.this.videoPath)));
            sharingIntent.putExtra("android.intent.extra.TEXT", "video");
            VideoViewActivity.this.startActivity(Intent.createChooser(sharingIntent, "Where to Share?"));
        }
    };
    ProgressDialog pd = null;
    Runnable runnbledelet = new Runnable() {
        public void run() {
            VideoViewActivity.this.pd.dismiss();
            VideoViewActivity.this.handler.removeCallbacks(VideoViewActivity.this.runnbledelet);
            Intent i = new Intent(VideoViewActivity.this, MainActivity.class);
            i.addFlags(335544320);
            VideoViewActivity.this.startActivity(i);
        }
    };
    SeekBar seekVideo;
    Runnable seekrunnable = new Runnable() {
        public void run() {
            if (VideoViewActivity.this.videoview.isPlaying()) {
                int curPos = VideoViewActivity.this.videoview.getCurrentPosition();
                VideoViewActivity.this.seekVideo.setProgress(curPos);
                try {
                    VideoViewActivity.this.tvStartVideo.setText(VideoViewActivity.formatTimeUnit((long) curPos));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                VideoViewActivity.this.handler.postDelayed(VideoViewActivity.this.seekrunnable, 500);
                return;
            }
            VideoViewActivity.this.handler.removeCallbacks(VideoViewActivity.this.seekrunnable);
        }
    };
    TextView tvEndVideo;
    TextView tvStartVideo;
    TextView tvVideoName;
    String videoPath = "";
    VideoView videoview;

    protected void onPause() {
        super.onPause();
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.videoview_layout);
        this.videoPath = getIntent().getStringExtra("videofilename");
        FindbyID();
        this.videoview.setVideoPath(this.videoPath);
        this.videoview.setOnErrorListener(new OnErrorListener() {
            public boolean onError(MediaPlayer mp, int what, int extra) {
                VideoViewActivity.this.isPlay = false;
                Toast.makeText(VideoViewActivity.this.getApplicationContext(), "Video Player Not Supproting", 0).show();
                return true;
            }
        });
        this.videoview.setOnPreparedListener(new OnPreparedListener() {
            public void onPrepared(MediaPlayer mp) {
                VideoViewActivity.this.isPlay = false;
                VideoViewActivity.this.duration = VideoViewActivity.this.videoview.getDuration();
                VideoViewActivity.this.seekVideo.setMax(VideoViewActivity.this.duration);
                VideoViewActivity.this.seekVideo.setProgress(0);
                VideoViewActivity.this.videoview.seekTo(100);
                VideoViewActivity.this.tvStartVideo.setText("00:00");
                try {
                    VideoViewActivity.this.tvEndVideo.setText(VideoViewActivity.formatTimeUnit((long) VideoViewActivity.this.duration));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        });
        this.videoview.setOnCompletionListener(new OnCompletionListener() {
            public void onCompletion(MediaPlayer mp) {
                VideoViewActivity.this.btnPlayVideo.setVisibility(0);
                VideoViewActivity.this.seekVideo.setProgress(0);
                VideoViewActivity.this.videoview.seekTo(100);
                VideoViewActivity.this.tvStartVideo.setText("00:00");
                VideoViewActivity.this.handler.removeCallbacks(VideoViewActivity.this.seekrunnable);
                VideoViewActivity.this.isPlay = false;
            }
        });
    }

    private void FindbyID() {
        this.videoview = (VideoView) findViewById(R.id.squarevideo);
        this.iVFrame = (ImageView) findViewById(R.id.ivVFrame);
        this.iVFrame.setOnClickListener(this.onclickplayvideo);
        this.btnShare = (Button) findViewById(R.id.btnShareVideo);
        this.btnShare.setOnClickListener(this.onclickbtnshareall);
        this.btnDelete = (Button) findViewById(R.id.buttonDelete);
        this.btnDelete.setOnClickListener(this.onclickDeleteVideo);
        this.seekVideo = (SeekBar) findViewById(R.id.sbVideo);
        this.seekVideo.setOnSeekBarChangeListener(this);
        this.tvStartVideo = (TextView) findViewById(R.id.tvStartVideo);
        this.tvEndVideo = (TextView) findViewById(R.id.tvEndVideo);
        this.btnPlayVideo = (Button) findViewById(R.id.btnPlayVideo);
        this.btnPlayVideo.setOnClickListener(this.onclickplayvideo);
    }

    @SuppressLint({"NewApi", "DefaultLocale"})
    @TargetApi(9)
    public static String formatTimeUnit(long millis) throws ParseException {
        return String.format("%02d:%02d", new Object[]{Long.valueOf(TimeUnit.MILLISECONDS.toMinutes(millis)), Long.valueOf(TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)))});
    }

    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
    }

    public void onStartTrackingTouch(SeekBar seekBar) {
    }

    public void onStopTrackingTouch(SeekBar seekBar) {
        int progress = seekBar.getProgress();
        this.videoview.seekTo(progress);
        try {
            this.tvStartVideo.setText(formatTimeUnit((long) progress));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public void onBackPressed() {
        if (this.videoview != null && this.videoview.isPlaying()) {
            this.videoview.pause();
        }
        Intent in = new Intent(this, MainActivity.class);
        in.addFlags(335544320);
        startActivity(in);
    }

    @SuppressLint({"NewApi"})
    protected void onDestroy() {
        if (this.videoview != null && this.videoview.isPlaying()) {
            this.videoview.pause();
        }
        super.onDestroy();
    }
}
