package com.mobyta.videoapp.merger;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mobyta.videoapp.R;

/**
 * Created by Sanchit on 08-03-2016.
 */
public class Custom_List_Demo extends BaseAdapter {
    private static LayoutInflater inflater = null;
    Context context;
    String[] result;
    int row = 1;

    public class Holder {
        ImageView imgImage;
        TextView txtText;
    }

    public Custom_List_Demo(Show_Video mainActivity, String[] prgmNameList) {
        this.result = prgmNameList;
        this.context = mainActivity;
        inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        return this.result.length;
    }

    public Object getItem(int position) {
        return Integer.valueOf(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder = new Holder();
        View rowView = inflater.inflate(R.layout.custom_list_view, null);
        holder.txtText = (TextView) rowView.findViewById(R.id.txtText);
        holder.imgImage = (ImageView) rowView.findViewById(R.id.imgAudio);
        holder.txtText.setText(this.result[position]);
        return rowView;
    }
}