package com.mobyta.videoapp.merger;

import android.os.Environment;

import java.io.File;
import java.io.FilenameFilter;
import java.util.Arrays;
import java.util.List;

public class FileUtils {

    static class AnonymousClass1 implements FilenameFilter {
        private final /* synthetic */ String val$fileName;

        AnonymousClass1(String str) {
            this.val$fileName = str;
        }

        public boolean accept(File dir, String filename) {
            return filename != null && filename.startsWith("mix-") && filename.endsWith(this.val$fileName);
        }
    }

    public static String getTargetFileName(String inputFileName) {
        String fileName = new File(inputFileName).getAbsoluteFile().getName();
        int count = 0;
        List<String> fileList = Arrays.asList(new File(Environment.getExternalStorageDirectory() + "/MixAudioVideo").getAbsoluteFile().list(new AnonymousClass1(fileName)));
        while (true) {
            StringBuilder stringBuilder = new StringBuilder("mix-");
            Object[] objArr = new Object[1];
            int count2 = count + 1;
            objArr[0] = Integer.valueOf(count);
            String targetFileName = stringBuilder.append(String.format("%03d", objArr)).append("-").append(fileName).toString();
            if (!fileList.contains(targetFileName)) {
                return new File(Environment.getExternalStorageDirectory() + "/MixAudioVideo", targetFileName).getPath();
            }
            count = count2;
        }
    }
}
