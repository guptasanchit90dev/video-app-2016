package com.mobyta.videoapp.merger;

/**
 * Created by Sanchit on 08-03-2016.
 */


import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore.Audio.Media;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;
import com.mobyta.videoapp.R;

import org.bytedeco.javacpp.opencv_core;
import org.bytedeco.javacv.FFmpegFrameGrabber;
import org.bytedeco.javacv.FFmpegFrameRecorder;
import org.bytedeco.javacv.Frame;
import org.bytedeco.javacv.FrameGrabber;
import org.bytedeco.javacv.FrameRecorder;

import java.io.File;


public class Show_Video extends Activity implements OnClickListener {
    private String PATH = opencv_core.cvFuncName;
    public AlertDialog alertDialog;
    public String[] au;
    public Button btnAudio;
    public Button btnSave;
    public ContentResolver contentResolver;
    public long currenttime = 0;
    public String finalsong;
    public long grad1;
    public long grad2;
    public ImageView imgIcon;
    public long lasttime = 0;
    public String[] mAudioPath;
    public Cursor musiccursor;
    public ListView musiclist;
    public String[] path;
    public String pathofvideo;
    public View promptsView;
    public String song;
    public String[] songs;
    public Bitmap thumbAsBitmap;
    public BitmapDrawable thumbAsDrawable;
    public long totalgrad = 0;
    public TextView tvText;
    public Uri uri;
    public VideoView vvShowVideo;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.show_video);
        init();
        bindView();
        addListener();
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    private void init() {
        this.PATH = new StringBuilder(String.valueOf(Environment.getExternalStorageDirectory().getPath())).append("/").append(getString(R.string.app_name)).toString();
        new File(this.PATH).mkdirs();
    }

    private void bindView() {
        this.pathofvideo = getIntent().getStringExtra("videoFilePath");
        this.btnAudio = (Button) findViewById(R.id.btnAudio);
        this.btnSave = (Button) findViewById(R.id.btnSave);
        this.imgIcon = (ImageView) findViewById(R.id.imgPlay);
        this.vvShowVideo = (VideoView) findViewById(R.id.vvShowVideo);
        this.promptsView = LayoutInflater.from(this).inflate(R.layout.custom_dialog, null);
        Builder alertDialogBuilder = new Builder(this);
        alertDialogBuilder.setView(this.promptsView);
        alertDialogBuilder.setTitle("Select Audio..");
        this.vvShowVideo.setVideoURI(Uri.parse(this.pathofvideo));
        this.vvShowVideo.setMediaController(new MediaController(this));
        MediaController mediaController = new MediaController(this, false) {
            public boolean dispatchKeyEvent(KeyEvent event) {
                if (event.getKeyCode() == 4) {
                    Show_Video.this.finish();
                }
                return super.dispatchKeyEvent(event);
            }
        };
        mediaController.show();
        mediaController.setAnchorView(this.vvShowVideo);
        this.vvShowVideo.setMediaController(mediaController);
        this.thumbAsBitmap = ThumbnailUtils.createVideoThumbnail(this.pathofvideo, 1);
        this.thumbAsDrawable = new BitmapDrawable(this.thumbAsBitmap);
        this.vvShowVideo.setBackgroundDrawable(this.thumbAsDrawable);
        this.vvShowVideo.pause();
        this.alertDialog = alertDialogBuilder.create();
        this.musiclist = (ListView) this.promptsView.findViewById(R.id.llSongList);
        this.path = getAudioList();
        this.musiclist.setAdapter(new Custom_List_Demo(this, this.path));

    }

    protected void onPause() {
        super.onPause();
        if (this.vvShowVideo != null && this.vvShowVideo.isPlaying()) {
            this.vvShowVideo.stopPlayback();
        }
    }

    private void addListener() {
        this.btnAudio.setOnClickListener(this);
        this.btnSave.setOnClickListener(this);
        this.musiclist.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
                Show_Video.this.finalsong = Show_Video.this.mAudioPath[arg2];
                Show_Video.this.createVideo();
            }
        });
        findViewById(R.id.ivGet_More).setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                Intent intentG = new Intent("android.intent.action.VIEW");
                intentG.setData(Uri.parse("market://search?q=pub:Willson Cop"));
                Show_Video.this.startActivity(intentG);
            }
        });
    }


    private String[] getAudioList() {
        Cursor mCursor = getContentResolver().query(Media.EXTERNAL_CONTENT_URI, new String[]{"_display_name", "_data"}, null, null, "LOWER(title) ASC");
        int count = mCursor.getCount();
        this.songs = new String[count];
        this.mAudioPath = new String[count];
        int i = 0;
        if (mCursor.moveToFirst()) {
            do {
                this.songs[i] = mCursor.getString(mCursor.getColumnIndexOrThrow("_display_name"));
                this.mAudioPath[i] = mCursor.getString(mCursor.getColumnIndexOrThrow("_data"));
                i++;
            } while (mCursor.moveToNext());
        }
        mCursor.close();
        return this.songs;
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgPlay:
                this.imgIcon.setVisibility(4);
                this.vvShowVideo.setBackgroundDrawable(null);
                this.vvShowVideo.start();
                return;
            case R.id.btnAudio:
                this.alertDialog.show();
                this.alertDialog.setCanceledOnTouchOutside(false);
                return;
            default:
                return;
        }
    }

    public void createVideo() {
        new AsyncTask<Void, Void, Boolean>() {
            ProgressDialog dialog;
            String fileName;

            protected void onPreExecute() {
                this.fileName = "VID_" + System.currentTimeMillis();
                Show_Video.this.vvShowVideo.pause();
                this.dialog = new ProgressDialog(Show_Video.this);
                this.dialog.setMessage("Mixing Audio in Video, Please wait.........");
                this.dialog.setCancelable(false);
                this.dialog.show();

                // Show_Video.this.alertDialog.dismiss();
            }

            protected Boolean doInBackground(Void... arg0) {
                FrameGrabber videoGrabber = new FFmpegFrameGrabber(Show_Video.this.pathofvideo);
                FrameGrabber audioGrabber = new FFmpegFrameGrabber(Show_Video.this.finalsong);

                try {
                    videoGrabber.start();
                    audioGrabber.start();

                    FrameRecorder recorder2 = new FFmpegFrameRecorder(
                            new StringBuilder(
                                    String.valueOf(
                                            Show_Video.this.PATH)).
                                    append("/").
                                    append(this.fileName).
                                    append(".3gp").toString(),
                            videoGrabber.getImageWidth(),
                            videoGrabber.getImageHeight(),
                            audioGrabber.getAudioChannels());

                    try {

                        recorder2.setFrameRate(videoGrabber.getFrameRate());
                        recorder2.setSampleRate(audioGrabber.getSampleRate());
                        recorder2.start();

                        Frame audioFrame;
                        Frame videoFrame;
                        while (true) {
                            audioFrame = audioGrabber.grabFrame();
                            videoFrame = videoGrabber.grabFrame();

                            if (videoFrame == null) {
                                break;
                            } else {
                                videoFrame.samples = null;
                            }

                            if (audioFrame == null) {
                                audioGrabber.stop();
                                audioGrabber.release();
                                audioGrabber = new FFmpegFrameGrabber(Show_Video.this.finalsong);
                                audioGrabber.start();
                                audioFrame = audioGrabber.grabFrame();
                            }

                            if (audioFrame != null) {
                                audioFrame.image = null;
                            }
                            recorder2.record(videoFrame);
                            recorder2.record(audioFrame);
                        }
                        recorder2.stop();
                        videoGrabber.stop();
                        audioGrabber.stop();
                        return true;
                    } catch (FrameRecorder.Exception e) {
                        Log.e("Error", e.getMessage());

                    }
                } catch (FrameGrabber.Exception ex) {
                    Log.e("Error", ex.getMessage());

                }
                return false;

            }

            protected void onPostExecute(Boolean result) {
                super.onPostExecute(result);
                if (this.dialog != null && this.dialog.isShowing()) {
                    this.dialog.dismiss();
                }
            }
        }.execute(new Void[0]);
    }

    public void createVideo2() {
        new AsyncTask<Void, Void, Boolean>() {
            ProgressDialog dialog;
            String fileName;

            protected void onPreExecute() {
                this.fileName = "VID_" + System.currentTimeMillis();
                Show_Video.this.vvShowVideo.pause();
                this.dialog = new ProgressDialog(Show_Video.this);
                this.dialog.setMessage("Mixing Audio in Video, Please wait.........");
                this.dialog.setCancelable(false);
                this.dialog.show();

                Show_Video.this.alertDialog.dismiss();
            }

            protected Boolean doInBackground(Void... arg0) {
                FrameGrabber videoGrabber = new FFmpegFrameGrabber(Show_Video.this.pathofvideo);
                FrameGrabber audioGrabber = new FFmpegFrameGrabber(Show_Video.this.finalsong);

                FrameRecorder recorder2 = new FFmpegFrameRecorder(
                        new StringBuilder(
                                String.valueOf(
                                        Show_Video.this.PATH)).
                                append("/").
                                append(this.fileName).
                                append(".3gp").toString(),
                        videoGrabber.getImageWidth(),
                        videoGrabber.getImageHeight(),
                        audioGrabber.getAudioChannels());

                try {
                    videoGrabber.start();
                    audioGrabber.start();

                    recorder2.setFrameRate(videoGrabber.getFrameRate());
                    recorder2.setSampleRate(audioGrabber.getSampleRate());
                    recorder2.start();

                    while (true) {
                        Frame audioFrame = audioGrabber.grabFrame();
                        Frame videoFrame = videoGrabber.grabFrame();
                        if (videoFrame == null) {
                            break;
                        }
                        if (audioFrame == null) {
                            audioGrabber.stop();
                            audioGrabber.release();
                            audioGrabber = new FFmpegFrameGrabber(Show_Video.this.finalsong);
                            audioGrabber.start();
                            audioFrame = audioGrabber.grabFrame();
                        }
                        if (audioFrame != null) {
                            audioFrame.image = null;
                        }

                        recorder2.record(videoFrame);
                        recorder2.record(audioFrame);
                    }
                    recorder2.stop();
                    videoGrabber.stop();
                    audioGrabber.stop();
                    return true;
                } catch (FrameGrabber.Exception ex) {
                    Log.e("Error", ex.getMessage());

                } catch (FrameRecorder.Exception e) {
                    Log.e("Error", e.getMessage());

                }
                return false;

            }

            protected void onPostExecute(Boolean result) {
                super.onPostExecute(result);
                if (this.dialog != null && this.dialog.isShowing()) {
                    this.dialog.dismiss();
                }
            }
        }.execute(new Void[0]);
    }

}
