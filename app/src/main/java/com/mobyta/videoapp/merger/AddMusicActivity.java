package com.mobyta.videoapp.merger;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.media.MediaScannerConnection;
import android.net.ParseException;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore.Audio.Media;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.VideoView;

import com.mobyta.videoapp.R;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;


public class AddMusicActivity extends Activity {
    public static final int RESULT_LOAD_AUDIO = 1;
    String audioPathName;
    AudioSliceSeekBar audioSliceSeekbar;
    Button btnCreateVideo;
    Button btnMusicClear;
    Handler handler = new Handler();
    ImageView imgScreen;
    LinearLayout ll;
    Context mContext;
    MediaPlayer mPlayer;
    OnClickListener mixAudioToVideo = new OnClickListener() {
        public void onClick(View v) {
            if (v == AddMusicActivity.this.btnCreateVideo) {
                AddMusicActivity.this.videoView.pause();
                if (AddMusicActivity.this.mPlayer != null) {
                    AddMusicActivity.this.mPlayer.pause();
                }
            }
            new MixAudioToVideo().execute(new Void[0]);
        }
    };
    OnClickListener onclickClearMusic = new OnClickListener() {
        public void onClick(View v) {
            if (AddMusicActivity.this.videoView != null && AddMusicActivity.this.videoView.isPlaying()) {
                AddMusicActivity.this.videoView.pause();
                AddMusicActivity.this.videoControlBtn.setVisibility(0);
                AddMusicActivity.this.plypush = Boolean.valueOf(false);
            }
            AddMusicActivity.this.audioPathName = "";
            AddMusicActivity.this.btnCreateVideo.setVisibility(8);
            AddMusicActivity.this.openAudioGallary.setVisibility(0);
            AddMusicActivity.this.ll.setVisibility(8);
            if (AddMusicActivity.this.mPlayer != null && AddMusicActivity.this.mPlayer.isPlaying()) {
                AddMusicActivity.this.mPlayer.pause();
                AddMusicActivity.this.mPlayer.release();
                AddMusicActivity.this.mPlayer = null;
            }
        }
    };
    Button openAudioGallary;
    OnClickListener openAudioGallaryFromSdCard = new OnClickListener() {
        public void onClick(View v) {
            if (AddMusicActivity.this.videoView != null && AddMusicActivity.this.videoView.isPlaying()) {
                AddMusicActivity.this.videoView.pause();
                AddMusicActivity.this.videoControlBtn.setVisibility(0);
                AddMusicActivity.this.plypush = Boolean.valueOf(false);
            }
            AddMusicActivity.this.startActivityForResult(new Intent("android.intent.action.PICK", Media.EXTERNAL_CONTENT_URI), AddMusicActivity.RESULT_LOAD_AUDIO);
        }
    };
    String outputPath;
    ProgressDialog pd = null;
    Boolean plypush = Boolean.valueOf(false);
    Runnable r = new Runnable() {
        public void run() {
            if (AddMusicActivity.this.pd != null && AddMusicActivity.this.pd.isShowing()) {
                AddMusicActivity.this.pd.dismiss();
            }
            AddMusicActivity.this.handler.removeCallbacks(AddMusicActivity.this.r);
            Intent intent = new Intent(AddMusicActivity.this, VideoViewActivity.class);
            intent.addFlags(335544320);
            intent.putExtra("videofilename", AddMusicActivity.this.outputPath);
            AddMusicActivity.this.startActivity(intent);
            System.exit(0);
        }
    };
    Uri selectedAudio;
    private TextView textViewLeft;
    private TextView textViewRight;
    private TextView tvAudTitle;
    private TextView tvEndAudio1;
    private TextView tvStrartAudio1;
    Uri uri = null;
    View videoControlBtn;
    private VideoPlayerState videoPlayerState = new VideoPlayerState();
    VideoSliceSeekBar videoSliceSeekBar;
    private StateObserver videoStateObserver = new StateObserver();
    VideoView videoView;

    class MixAudioToVideo extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            AddMusicActivity.this.pd = new ProgressDialog(AddMusicActivity.this.mContext);
            AddMusicActivity.this.pd.setMessage("Adding Audio...");
            AddMusicActivity.this.pd.setCancelable(false);
            AddMusicActivity.this.pd.show();
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            AddMusicActivity.this.outputPath = Environment.getExternalStorageDirectory().getAbsoluteFile() + "/" + AddMusicActivity.this.getResources().getString(R.string.app_folder_name);
            File file = new File(AddMusicActivity.this.outputPath);
            if (!file.exists()) {
                file.mkdirs();
            }
            AddMusicActivity.this.outputPath = file.getAbsolutePath() + "/Mix_" + System.currentTimeMillis() + ".mkv";
            int start = AddMusicActivity.this.videoPlayerState.getStart() / 1000;
            int duration = AddMusicActivity.this.videoPlayerState.getDuration() / 1000;
            int startAudio = 0;
/*
            startAudio = AddMusicActivity.this.audioSliceSeekbar.getLeftProgress();
*/
            int audioStartTime = startAudio / 1000;
            String inputFileName = AddMusicActivity.this.videoPlayerState.getFilename();
            FFmpegcmd.ffmpeg("ffmpeg", "-y", "-ss", String.valueOf(start), "-t", String.valueOf(duration), "-i", inputFileName, "-ss", String.valueOf(audioStartTime), "-i", AddMusicActivity.this.audioPathName, "-map", "0:0", "-map", "1:0", "-acodec", "copy", "-vcodec", "copy", "-preset", "ultrafast", "-ss", "0", "-t", String.valueOf(duration), AddMusicActivity.this.outputPath);
            return Boolean.valueOf(true);
        }

        @Override
        protected void onPostExecute(Boolean result) {
            AddMusicActivity.this.handler.postDelayed(AddMusicActivity.this.r, 1000);
            Context context = AddMusicActivity.this.mContext;
            String[] strArr = new String[AddMusicActivity.RESULT_LOAD_AUDIO];
            strArr[0] = AddMusicActivity.this.outputPath;
            String[] strArr2 = new String[AddMusicActivity.RESULT_LOAD_AUDIO];
            strArr2[0] = "mkv";
            MediaScannerConnection.scanFile(context, strArr, strArr2, null);
        }
    }

    private class StateObserver extends Handler {
        private boolean alreadyStarted;
        private Runnable observerWork;

        private StateObserver() {
            this.alreadyStarted = false;
            this.observerWork = new Runnable() {
                public void run() {
                    StateObserver.this.startVideoProgressObserving();
                }
            };
        }

        private void startVideoProgressObserving() {
            if (!this.alreadyStarted) {
                this.alreadyStarted = true;
                sendEmptyMessage(0);
            }
        }

        public void handleMessage(Message msg) {
            this.alreadyStarted = false;
            AddMusicActivity.this.videoSliceSeekBar.videoPlayingProgress(AddMusicActivity.this.videoView.getCurrentPosition());
            if (AddMusicActivity.this.mPlayer != null && AddMusicActivity.this.mPlayer.isPlaying()) {
                AddMusicActivity.this.audioSliceSeekbar.videoPlayingProgress(AddMusicActivity.this.mPlayer.getCurrentPosition());
            }
            if (!AddMusicActivity.this.videoView.isPlaying() || AddMusicActivity.this.videoView.getCurrentPosition() >= AddMusicActivity.this.videoSliceSeekBar.getRightProgress()) {
                if (AddMusicActivity.this.videoView.isPlaying()) {
                    AddMusicActivity.this.videoView.pause();
                    AddMusicActivity.this.videoControlBtn.setVisibility(0);
                    AddMusicActivity.this.plypush = Boolean.valueOf(false);
                }
                AddMusicActivity.this.videoSliceSeekBar.setSliceBlocked(false);
                AddMusicActivity.this.videoSliceSeekBar.removeVideoStatusThumb();
                if (AddMusicActivity.this.mPlayer != null && AddMusicActivity.this.mPlayer.isPlaying()) {
                    AddMusicActivity.this.mPlayer.pause();
                    AddMusicActivity.this.audioSliceSeekbar.setSliceBlocked(false);
                    AddMusicActivity.this.audioSliceSeekbar.removeVideoStatusThumb();
                    return;
                }
                return;
            }
            postDelayed(this.observerWork, 50);
        }
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.addmusic_layout);
        this.mContext = this;
        FindByID();
        Object lastState = getLastNonConfigurationInstance();
        if (lastState != null) {
            this.videoPlayerState = (VideoPlayerState) lastState;
        } else {
            this.videoPlayerState.setFilename(getIntent().getExtras().getString("videoFilePath"));
        }
        initVideoView();
    }

    private void FindByID() {
        this.textViewLeft = (TextView) findViewById(R.id.left_pointer);
        this.textViewRight = (TextView) findViewById(R.id.right_pointer);
        this.audioSliceSeekbar = (AudioSliceSeekBar) findViewById(R.id.audioSeekBar);
        this.videoSliceSeekBar = (VideoSliceSeekBar) findViewById(R.id.seekBar1);
        this.videoView = (VideoView) findViewById(R.id.videoView1);
        this.imgScreen = (ImageView) findViewById(R.id.ivScreen);
        this.videoControlBtn = findViewById(R.id.btnPlayVideo);
        this.ll = (LinearLayout) findViewById(R.id.llLinear);
        this.tvStrartAudio1 = (TextView) findViewById(R.id.tvStartAudio);
        this.tvEndAudio1 = (TextView) findViewById(R.id.tvEndAudio);
        this.tvAudTitle = (TextView) findViewById(R.id.tvAudioTitle);
        this.openAudioGallary = (Button) findViewById(R.id.btnAudioSelection);
        this.openAudioGallary.setOnClickListener(this.openAudioGallaryFromSdCard);
        this.btnCreateVideo = (Button) findViewById(R.id.btnCreateVideo);
        this.btnCreateVideo.setOnClickListener(this.mixAudioToVideo);
        this.btnMusicClear = (Button) findViewById(R.id.btnClearMusic);
        this.btnMusicClear.setOnClickListener(this.onclickClearMusic);
    }

    private void initVideoView() {
        this.videoView.setOnPreparedListener(new OnPreparedListener() {
            public void onPrepared(final MediaPlayer mp) {
                AddMusicActivity.this.videoSliceSeekBar.setSeekBarChangeListener(new VideoSliceSeekBar.SeekBarChangeListener() {
                    public void SeekBarValueChanged(int leftThumb, int rightThumb) {
                        if (AddMusicActivity.this.videoSliceSeekBar.getSelectedThumb() == AddMusicActivity.RESULT_LOAD_AUDIO) {
                            AddMusicActivity.this.videoView.seekTo(AddMusicActivity.this.videoSliceSeekBar.getLeftProgress());
                        }
                        AddMusicActivity.this.textViewLeft.setText(AddMusicActivity.formatTimeUnit((long) leftThumb, true));
                        AddMusicActivity.this.textViewRight.setText(AddMusicActivity.formatTimeUnit((long) rightThumb, true));
                        AddMusicActivity.this.videoPlayerState.setStart(leftThumb);
                        AddMusicActivity.this.videoPlayerState.setStop(rightThumb);
                        if (AddMusicActivity.this.mPlayer != null && AddMusicActivity.this.mPlayer.isPlaying()) {
                            AddMusicActivity.this.mPlayer.seekTo(AddMusicActivity.this.audioSliceSeekbar.getLeftProgress());
                            AddMusicActivity.this.audioSliceSeekbar.videoPlayingProgress(AddMusicActivity.this.audioSliceSeekbar.getLeftProgress());
                            AddMusicActivity.this.mPlayer.start();
                        }
                        mp.setVolume(0.0f, 0.0f);
                    }
                });
                AddMusicActivity.this.videoSliceSeekBar.setMaxValue(mp.getDuration());
                AddMusicActivity.this.videoSliceSeekBar.setLeftProgress(0);
                AddMusicActivity.this.videoSliceSeekBar.setRightProgress(mp.getDuration());
                AddMusicActivity.this.videoSliceSeekBar.setProgressMinDiff(0);
                AddMusicActivity.this.videoControlBtn.setOnClickListener(new OnClickListener() {
                    public void onClick(View v) {
                        if (AddMusicActivity.this.mPlayer != null) {
                            AddMusicActivity.this.mPlayer.start();
                        }
                        if (AddMusicActivity.this.plypush.booleanValue()) {
                            AddMusicActivity.this.videoControlBtn.setVisibility(0);
                            AddMusicActivity.this.plypush = Boolean.valueOf(false);
                        } else {
                            AddMusicActivity.this.videoControlBtn.setVisibility(8);
                            AddMusicActivity.this.plypush = Boolean.valueOf(true);
                        }
                        AddMusicActivity.this.performVideoViewClick();
                    }
                });
            }
        });
        this.videoView.setVideoPath(this.videoPlayerState.getFilename());
        this.videoView.setOnTouchListener(new OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if (AddMusicActivity.this.plypush.booleanValue()) {
                    AddMusicActivity.this.videoView.pause();
                    if (AddMusicActivity.this.mPlayer != null) {
                        AddMusicActivity.this.mPlayer.pause();
                    }
                    AddMusicActivity.this.plypush = Boolean.valueOf(false);
                    AddMusicActivity.this.videoControlBtn.setVisibility(0);
                }
                return true;
            }
        });
    }

    private void performVideoViewClick() {
        if (this.videoView.isPlaying()) {
            this.videoView.pause();
            this.videoSliceSeekBar.setSliceBlocked(true);
            this.videoSliceSeekBar.removeVideoStatusThumb();
            if (this.mPlayer != null && this.mPlayer.isPlaying()) {
                this.mPlayer.pause();
                this.audioSliceSeekbar.setSliceBlocked(true);
                this.audioSliceSeekbar.removeVideoStatusThumb();
                return;
            }
            return;
        }
        this.videoView.seekTo(this.videoSliceSeekBar.getLeftProgress());
        this.videoView.start();
        this.videoSliceSeekBar.videoPlayingProgress(this.videoSliceSeekBar.getLeftProgress());
        this.videoStateObserver.startVideoProgressObserving();
        if (this.mPlayer != null && this.mPlayer.isPlaying()) {
            this.mPlayer.seekTo(this.audioSliceSeekbar.getLeftProgress());
            this.audioSliceSeekbar.videoPlayingProgress(this.audioSliceSeekbar.getLeftProgress());
            this.mPlayer.start();
        }
    }

    @SuppressLint({"NewApi"})
    public static String formatTimeUnit1(long millis) throws ParseException {
        return String.format("%02d:%02d", new Object[]{Long.valueOf(TimeUnit.MILLISECONDS.toMinutes(millis)), Long.valueOf(TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)))});
    }

    @SuppressLint({"NewApi"})
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESULT_LOAD_AUDIO && resultCode == -1 && data != null) {
            this.ll.setVisibility(0);
            this.openAudioGallary.setVisibility(8);
            this.tvStrartAudio1.setVisibility(0);
            this.tvEndAudio1.setVisibility(0);
            this.tvAudTitle.setVisibility(0);
            this.btnCreateVideo.setVisibility(0);
            this.selectedAudio = data.getData();
            String[] filePathColumn = new String[]{"_data", "_display_name"};
            Cursor cursor = getContentResolver().query(this.selectedAudio, filePathColumn, null, null, null);
            cursor.moveToFirst();
            this.audioPathName = cursor.getString(cursor.getColumnIndex(filePathColumn[0]));
            this.tvAudTitle.setText(cursor.getString(cursor.getColumnIndex("_display_name")));
            cursor.close();
            this.mPlayer = new MediaPlayer();
            try {
                this.mPlayer.setDataSource(this.audioPathName);
                this.mPlayer.prepare();
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (SecurityException e2) {
                e2.printStackTrace();
            } catch (IllegalStateException e3) {
                e3.printStackTrace();
            } catch (IOException e4) {
                e4.printStackTrace();
            }
            this.mPlayer.setOnPreparedListener(new OnPreparedListener() {
                public void onPrepared(MediaPlayer mplayer) {
                    AddMusicActivity.this.audioSliceSeekbar.setSeekBarChangeListener(new AudioSliceSeekBar.SeekBarChangeListener() {
                        public void SeekBarValueChanged(int leftThumb, int rightThumb) {
                            if (AddMusicActivity.this.audioSliceSeekbar.getSelectedThumb() == AddMusicActivity.RESULT_LOAD_AUDIO) {
                                AddMusicActivity.this.mPlayer.seekTo(AddMusicActivity.this.audioSliceSeekbar.getLeftProgress());
                            }
                            AddMusicActivity.this.tvStrartAudio1.setText(AddMusicActivity.formatTimeUnit((long) leftThumb, true));
                            AddMusicActivity.this.tvEndAudio1.setText(AddMusicActivity.formatTimeUnit((long) rightThumb, true));
                            if (AddMusicActivity.this.videoView != null && AddMusicActivity.this.videoView.isPlaying()) {
                                AddMusicActivity.this.videoView.seekTo(AddMusicActivity.this.videoSliceSeekBar.getLeftProgress());
                                AddMusicActivity.this.videoView.start();
                                AddMusicActivity.this.videoSliceSeekBar.videoPlayingProgress(AddMusicActivity.this.videoSliceSeekBar.getLeftProgress());
                                AddMusicActivity.this.videoStateObserver.startVideoProgressObserving();
                            }
                        }
                    });
                    AddMusicActivity.this.audioSliceSeekbar.setMaxValue(mplayer.getDuration());
                    AddMusicActivity.this.audioSliceSeekbar.setLeftProgress(0);
                    AddMusicActivity.this.audioSliceSeekbar.setRightProgress(mplayer.getDuration());
                    AddMusicActivity.this.audioSliceSeekbar.setProgressMinDiff(0);
                    AddMusicActivity.this.tvStrartAudio1.setText("00:00");
                    try {
                        AddMusicActivity.this.tvEndAudio1.setText(AddMusicActivity.formatTimeUnit1((long) mplayer.getDuration()));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
            this.mPlayer.setOnErrorListener(new OnErrorListener() {
                public boolean onError(MediaPlayer mp, int what, int extra) {
                    return true;
                }
            });
        }
    }

    @SuppressLint({"NewApi"})
    public static String formatTimeUnit(long millis, boolean nn) throws ParseException {
        return String.format("%02d:%02d", new Object[]{Long.valueOf(TimeUnit.MILLISECONDS.toMinutes(millis)), Long.valueOf(TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)))});
    }

    public static String getTimeForTrackFormat(int timeInMills, boolean display2DigitsInMinsSection) {
        int hour = timeInMills / 3600000;
        int minutes = timeInMills / 60000;
        int seconds = (timeInMills - ((minutes * 60) * 1000)) / 1000;
        String result = (!display2DigitsInMinsSection || hour >= 10) ? "" : "0";
        StringBuilder stringBuilder = new StringBuilder(String.valueOf(new StringBuilder(String.valueOf(result)).append(hour).append(":").toString()));
        String str = (!display2DigitsInMinsSection || minutes >= 10) ? "" : "0";
        result = new StringBuilder(String.valueOf(stringBuilder.append(str).toString())).append(minutes % 60).append(":").toString();
        if (seconds < 10) {
            return new StringBuilder(String.valueOf(result)).append("0").append(seconds).toString();
        }
        return new StringBuilder(String.valueOf(result)).append(seconds).toString();
    }

    public void onBackPressed() {
        if (this.videoView != null && this.videoView.isPlaying()) {
            this.videoView.pause();
            if (this.mPlayer != null) {
                this.mPlayer.pause();
            }
        }
        Intent in = new Intent(this, MainActivity.class);
        in.addFlags(335544320);
        startActivity(in);
        finish();
    }
}
