package com.mobyta.videoapp.merger;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap.Config;
import android.net.Uri;
import android.provider.MediaStore.Video.Media;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.ResourceCursorAdapter;

import com.mobyta.videoapp.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions.Builder;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.SimpleBitmapDisplayer;


public final class MyVideoCursorAdapter extends ResourceCursorAdapter {
    Context ctx;
    ImageLoader imageLoader;

    public MyVideoCursorAdapter(Context context, int layout, Cursor c, ImageLoader imgloader) {
        super(context, layout, c);
        this.ctx = context;
        this.imageLoader = imgloader;
    }

    @SuppressLint({"NewApi"})
    public void bindView(View view, Context context, final Cursor cursor) {
        ImageView videoPreview = (ImageView) view.findViewById(R.id.thumbImage);
        Uri uri = Uri.withAppendedPath(Media.EXTERNAL_CONTENT_URI, getLong(cursor));
        this.imageLoader.displayImage(uri.toString(), videoPreview, new Builder().showImageForEmptyUri((int) R.color.trans).cacheInMemory(true).cacheOnDisc(true).imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2).bitmapConfig(Config.ARGB_8888).delayBeforeLoading(100).displayer(new SimpleBitmapDisplayer()).build());
        view.setTag(Integer.valueOf(cursor.getPosition()));
        view.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                cursor.moveToPosition(Integer.parseInt(String.valueOf(v.getTag())));
                String filepath = MyVideoCursorAdapter.this.getString(cursor, "_data");
                Intent intent = new Intent(MyVideoCursorAdapter.this.ctx, VideoViewActivity.class);
                intent.putExtra("videofilename", filepath);
                MyVideoCursorAdapter.this.ctx.startActivity(intent);
            }
        });
    }

    private String getString(Cursor cursor, String columnName) {
        return cursor.getString(cursor.getColumnIndexOrThrow(columnName));
    }

    private String getLong(Cursor cursor) {
        return String.valueOf(cursor.getLong(cursor.getColumnIndexOrThrow("_id")));
    }
}
