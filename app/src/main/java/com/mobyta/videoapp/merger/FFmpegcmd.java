package com.mobyta.videoapp.merger;

import android.content.Context;
import android.util.Log;

import com.mobyta.videoapp.R;

import java.io.File;
import java.io.IOException;

public class FFmpegcmd {
    public static native int ffmpeg(String... strArr);

    static {

        System.loadLibrary("mixav");
    }
}
