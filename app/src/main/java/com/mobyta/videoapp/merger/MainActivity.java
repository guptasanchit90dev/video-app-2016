package com.mobyta.videoapp.merger;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap.Config;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore.Video.Media;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.GridView;

import com.mobyta.videoapp.R;
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;


public class MainActivity extends Activity {
    public static final int RESULT_LOAD_VIDEO = 1;
    Button btnShareApp;
    Button btnStart;
    GridView gvMyVideo;
    ImageLoader imgLoader;
    Context mContext;
    OnClickListener onclickShareApp = new OnClickListener() {
        public void onClick(View v) {
            try {
                Intent i = new Intent("android.intent.action.SEND");
                i.setType("text/plain");
                i.putExtra("android.intent.extra.SUBJECT", MainActivity.this.getResources().getString(R.string.app_name));
                i.putExtra("android.intent.extra.TEXT", "\nAdd Audio to Video..\n\n" + "https://play.google.com/store/apps/details?id=add.audio.tovideo \n\n");
                MainActivity.this.startActivity(Intent.createChooser(i, "Share Application"));
            } catch (Exception e) {
            }
        }
    };
    OnClickListener onclickStart = new OnClickListener() {
        public void onClick(View v) {
            MainActivity.this.startActivityForResult(new Intent("android.intent.action.PICK", Media.EXTERNAL_CONTENT_URI), MainActivity.RESULT_LOAD_VIDEO);
        }
    };

    class loadCursordata extends AsyncTask<Void, Void, Boolean> {
        Cursor ecursor = null;
        ProgressDialog pd = null;

        loadCursordata() {
        }

        protected void onPreExecute() {
            this.pd = new ProgressDialog(MainActivity.this.mContext);
            this.pd.setMessage("Loading Videos...");
            this.pd.setCancelable(false);
            this.pd.show();
        }

        protected Boolean doInBackground(Void... arg0) {
            String MEDIA_DATA = "_data";
            String[] whereData = new String[MainActivity.RESULT_LOAD_VIDEO];
            whereData[0] = "%" + MainActivity.this.getResources().getString(R.string.app_folder_name) + "%";
            this.ecursor = MainActivity.this.managedQuery(Media.EXTERNAL_CONTENT_URI, new String[]{"_id", "_data", "_display_name", "_size", "duration", "date_added"}, "_data like ? ", whereData, "datetaken DESC");
            this.ecursor.moveToFirst();
            return Boolean.valueOf(true);
        }

        protected void onPostExecute(Boolean result) {
            if (this.pd != null && this.pd.isShowing()) {
                this.pd.dismiss();
            }
            MainActivity.this.gvMyVideo.setAdapter(new MyVideoCursorAdapter(MainActivity.this.mContext, R.layout.row_video_listadapter, this.ecursor, MainActivity.this.imgLoader));
        }
    }

    protected void onPause() {
        super.onPause();
    }

    public void onBackPressed() {
        System.exit(0);
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_layout);
        this.mContext = this;
        initImageLoader();
        findViewById();
        new loadCursordata().execute(new Void[0]);
    }

    private void findViewById() {
        this.btnStart = (Button) findViewById(R.id.btnStart);
        this.btnStart.setOnClickListener(this.onclickStart);
        this.gvMyVideo = (GridView) findViewById(R.id.gvVideoGrid);
        this.btnShareApp = (Button) findViewById(R.id.btnShareApp);
        this.btnShareApp.setOnClickListener(this.onclickShareApp);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESULT_LOAD_VIDEO && resultCode == -1 && data != null) {
            Uri selectedVideo = data.getData();
            String[] filePathColumn = new String[RESULT_LOAD_VIDEO];
            filePathColumn[0] = "_data";
            Cursor videoCursor = getContentResolver().query(selectedVideo, filePathColumn, null, null, null);
            videoCursor.moveToFirst();
            String videoPath = videoCursor.getString(videoCursor.getColumnIndex(filePathColumn[0]));
            videoCursor.close();
            Intent intent = new Intent(this, Show_Video.class);
            intent.putExtra("videoFilePath", videoPath);
            startActivity(intent);
        }
    }

    protected void onDestroy() {
        super.onDestroy();
        if (this.imgLoader != null) {
            this.imgLoader.clearDiscCache();
            this.imgLoader.clearMemoryCache();
        }
    }

    private void initImageLoader() {
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext()).memoryCache(new WeakMemoryCache()).defaultDisplayImageOptions(new DisplayImageOptions.Builder().cacheInMemory().cacheOnDisc().bitmapConfig(Config.RGB_565).displayer(new FadeInBitmapDisplayer(400)).build()).build();
        this.imgLoader = ImageLoader.getInstance();
        this.imgLoader.init(config);
    }
}
